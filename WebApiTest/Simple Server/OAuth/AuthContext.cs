﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SimpleServer.OAuth
{
    public class AuthContext : IdentityDbContext<ApplicationUser>
    {
        public AuthContext() : base("name=SimpleServer3") { }


        public static AuthContext Create()
        {
            return new AuthContext();
        }

        static AuthContext()
        {
            System.Data.Entity.Database.SetInitializer(new ApplicationDBInitializer());
        }


    }

    public class ApplicationDBInitializer : DropCreateDatabaseIfModelChanges<AuthContext>
    {
    }
}
