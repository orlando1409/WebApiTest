﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SimpleServer.OAuth
{
    public class ApplicationUser : IdentityUser
    {
        //new properties
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here

            userIdentity.AddClaim(new Claim("fullname", string.Format("{0} {1}", this.FirstName, this.LastName)));
            //userIdentit
            return userIdentity;
        }
    }

    public class ApplicationRole : IdentityRole
    {
        
    }
}