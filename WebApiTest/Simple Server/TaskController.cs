﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SimpleServer
{
    [RoutePrefix("api/task")]
    public class TaskController : ApiController
    {
        [Route("")]
        public IHttpActionResult Get(string name)
        {
            return Ok("Hello " + name);
        }

        [Route("")]
        public IHttpActionResult Post(Client client)
        {
            if (client.Type == "External")
                return BadRequest("Only Internal Clients");
            if (client.Type != "Internal") return BadRequest("Invalid Arguments");

            return Ok();
        }
    }
}
